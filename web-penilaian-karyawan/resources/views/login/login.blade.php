<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="{{asset('template/assets/images/logos/favicon.png')}}" />
    <link rel="stylesheet" href="{{asset('template/assets/css/styles.min.css')}}" />
    <title>Document</title>
</head>
<style>
    .login-box{
        border: solid 1px;
        width: 500px
    }
</style>
<body>
    <div class="vh-100 d-flex justify-content-center align-items-center flex-column">
        @if (Session::has('status'))
            <div class="alert alert-danger">
                {{ Session::get('message') }}
            </div>
        @endif

        <div class="login-box p-3">
            <form action="/login" method="POST">
                @csrf
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Email address</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email">
                </div>
                @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Password</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>
                @error('password')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary form-control">Submit</button>
              </form>
        </div>
        
    </div>



    <script src="{{asset('template/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('template/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('template/assets/js/sidebarmenu.js')}}"></script>
    <script src="{{asset('template/assets/js/app.min.js')}}"></script>
    <script src="{{asset('template/assets/libs/apexcharts/dist/apexcharts.min.js')}}"></script>
    <script src="{{asset('template/assets/libs/simplebar/dist/simplebar.js')}}"></script>
    <script src="{{asset('tamplate/assets/js/dashboard.js')}}"></script>
</body>
</html>