@extends('layout.master')

@section('title')
    Score | Edit
@endsection

@section('content')
    <h1>Edit Score</h1>
    <form action="/score/{{ $scores->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
          <label for="name" class="form-label">Employee Name : </label>
          <input type="text" class="form-control" id="name" readonly value="{{ $scores->employees->name }}">
          <input type="text" class="form-control" hidden value="{{ $scores->employees->id }}" name="employee_id">
        </div>
        @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    
        <div class="mb-3">
            <label for="user_id" class="form-label">Evaluator Name</label>
            <input type="integer" class="form-control" id="user_id" readonly value="{{ Auth::user()->user_name }}">
            <input type="text" class="form-control" name="user_id" hidden value="{{ Auth::user()->id }}">
          </div>
          @error('start_date')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
    
        <div class="mb-3">
          <label for="start_date" class="form-label">Start Date</label>
          <input type="date" class="form-control" id="start_date" name="start_date" value="{{ $scores->start_date }}">
        </div>
        @error('start_date')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    
        <div class="mb-3">
          <label for="end_date" class="form-label">End Date</label>
          <input type="date" class="form-control" id="end_date" name="end_date" value="{{ $scores->end_date }}">
        </div>
        @error('end_date')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    
        <div class="mb-3">
          <label for="value" class="form-label">Value</label>
          <input type="integer" class="form-control" id="value" name="value" value="{{ $scores->value }}">
        </div>
        @error('end_date')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    
        <div class="mb-3">
          <label for="note" class="form-label">Note</label>
          <textarea name="note" id="note" class="form-control">{{ $scores->note }}</textarea>
          
        </div>
        @error('note')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <button type="submit" class="btn btn-primary">Add Score</button>
      </form>
@endsection