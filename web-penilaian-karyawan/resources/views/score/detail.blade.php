@extends('layout.master')

@section('title')
    Score | Detail
@endsection

@section('content')
<div class="container row">
    <div class="card col-12" >
        <img src="{{ asset('image/'.$employees->photo) }}" class="card-img-top mx-auto" alt="..." style="width: 30rem;">
        <div class="card-body">
          <h5 class="card-title">{{ $employees->name }}</h5>
          <p class="card-text">{{ $employees->address }}</p>
          <p class="card-text">{{ $employees->date_of_birth }}</p>
          <p class="card-text">{{ $employees->nik }}</p>
          <p class="card-text">{{ $employees->phone }}</p>
          <p class="card-text">{{ $employees->email }}</p>
          <p class="card-text">@if ( $employees->gender  == "F" )
              Female
          @else
              Men
          @endif</p>
          <p class="card-text">{{ $employees->education }}</p>
          <p class="card-text">{{ $employees->division->division_name }}</p>
          <p class="card-text">{{ $employees->status->status_name }}</p>
          <p class="card-text">{{ $employees->position->position_name }}</p>
          
        </div>
    </div>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Avaluator Name</th>
            <th scope="col">Position</th>
            <th scope="col">Start Date</th>
            <th scope="col">End Date</th>
            <th scope="col">Score</th>
            <th scope="col">Note</th>
            @if (Auth::user()->profiles->position == "Admin")
            <th scope="col">Action</th>    
            @endif
            
          </tr>
        </thead>
        <tbody>
            @forelse ($employees->scores as $key=>$score)
          <tr>
            <th scope="row">{{ $key+1 }}</th>
            <td>{{ $score->users->profiles->name }}</td>    
            <td>{{ $score->users->profiles->position }}</td>    
            <td>{{ $score->start_date }}</td>    
            <td>{{ $score->end_date }}</td>    
            <td>{{ $score->value }}</td>    
            <td>{{ $score->note }}</td>
            @if (Auth::user()->profiles->position == "Admin")
            <td>
                <a href="/score/{{ $score->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                <form action="/score/{{ $score->id }}" method="POST" class="d-inline">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button>
                  </form>
            </td>                   
            @endif
 
          </tr>
          @empty
           <h1>No Score Data</h1>      
          @endforelse
        </tbody>
      </table>
      <a href="/score" class="btn btn-primary d-inline">Back To Score List</a>
</div>
@endsection