@extends('layout.master')

@section('title')
    Score
@endsection

@section('content')
    <div class="container">
        <h1>Employee List</h1>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">NIK</th>
                <th scope="col">Division</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($employees as $key=>$employee)
              <tr>
                <th scope="row">{{ $key+1 }}</th>
                <td>{{ $employee->name }}</td>
                <td>{{ $employee->nik }}</td>
                <td>{{ $employee->division->division_name }}</td>
                <td>
                  <a href="/score-add/{{ $employee->id }}" class="btn btn-primary btn-sm">Add Score</a>
                  <a href="/score-detail/{{ $employee->id }}" class="btn btn-success btn-sm">Detail Score</a>
                </td>
              </tr>                  
              @empty
                  <h1>No Data</h1>
              @endforelse

            </tbody>
          </table>
          <div>
            {{ $employees->links() }}
          </div>
    </div>
    
    {{-- <div class="container">
        <h1>List Score Employees</h1>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Evaluator Name</th>
                <th scope="col">Employee Name</th>
                <th scope="col">Start Date</th>
                <th scope="col">End Date</th>
                <th scope="col">Value</th>
                <th scope="col">Note</th>
                @if (Auth::user()->profiles->position == "Admin")
                <th scope="col">Action</th>    
                @endif
                

              </tr>
            </thead>
            <tbody>
              @forelse ($scores as $key=>$score)
              <tr>
                <th scope="row">{{ $key+1 }}</th>
                <td>{{ $score->users->user_name }}</td>
                <td>{{ $score->employees->name }}</td>
                <td>{{ $score->start_date }}</td>
                <td>{{ $score->end_date }}</td>
                <td>{{ $score->value }}</td>
                <td>{{ $score->note }}</td>
                @if (Auth::user()->profiles->position == "Admin")
                <td>
                  <a href="/score/{{ $score->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <form action="/score/{{ $score->id }}" method="POST" class="d-inline">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button>
                  </form>
                </td>                    
                @endif

              </tr>                  
              @empty
                  <h1>No Data</h1>
              @endforelse
            </tbody>
          </table>
          <div>
            {{ $scores->links() }}
          </div>
    </div> --}}
    @include('sweetalert::alert')
@endsection