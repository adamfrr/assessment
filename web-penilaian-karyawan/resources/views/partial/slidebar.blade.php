<!-- Sidebar Start -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div>
      <div class="brand-logo d-flex align-items-center justify-content-between">
        <a href="./index.html" class="text-nowrap logo-img">
          <img src="{{asset('template/assets/images/logos/dark-logo.svg')}}" width="180" alt="" />
        </a>
        <div class="close-btn d-xl-none d-block sidebartoggler cursor-pointer" id="sidebarCollapse">
          <i class="ti ti-x fs-8"></i>
        </div>
      </div>
      <!-- Sidebar navigation-->
      <nav class="sidebar-nav scroll-sidebar" data-simplebar="">
        <ul id="sidebarnav">
          <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">Home</span>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="/" aria-expanded="false">
              <span>
                <i class="ti ti-layout-dashboard"></i>
              </span>
              <span class="hide-menu">Dashboard</span>
            </a>
          </li>
          @if (Auth::user()->profiles->position == "Admin")
          <li class="sidebar-item">
            <a class="sidebar-link" href="/user" aria-expanded="false">
              <span>
                <i class="ti ti-user-plus"></i>
              </span>
              <span class="hide-menu">User</span>
            </a>
          </li>    
          @endif
          
          <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">Update Data</span>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="/score" aria-expanded="false">
              <span>
                <i class="ti ti-book"></i>
              </span>
              <span class="hide-menu">Scores</span>
            </a>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="/employee" aria-expanded="false">
              <span>
                <i class="ti ti-users"></i>
              </span>
              <span class="hide-menu">Employees</span>
            </a>
          </li>          
          @if (Auth::user()->profiles->position == "Admin")

          <li class="sidebar-item">
            <a class="sidebar-link" href="/status" aria-expanded="false">
              <span>
                <i class="ti ti-pencil"></i>
              </span>
              <span class="hide-menu">Statuses</span>
            </a>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="/position" aria-expanded="false">
              <span>
                <i class="ti ti-clipboard"></i>
              </span>
              <span class="hide-menu">Positions</span>
            </a>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="/division" aria-expanded="false">
              <span>
                <i class="ti ti-briefcase"></i>
              </span>
              <span class="hide-menu">Divisions</span>
            </a>
          </li>    
          @endif
          
          
          <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">AUTH</span>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="/logout" aria-expanded="false">
              <span>
                <i class="ti ti-login"></i>
              </span>
              <span class="hide-menu">Logout</span>
            </a>
          </li>
          
        </ul>
      </nav>
      <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
  </aside>
  <!--  Sidebar End -->