@extends('layout.master')

@section('title')
    Dashboard
@endsection

@section('content')
    <h1>Welcome {{ Auth::user()->profiles->name }}</h1>
    <div class="card" >
        <div class="card-body">
          <h5 class="card-title">Name : {{ Auth::user()->profiles->name }}</h5>
          <h5 class="card-title">UserName : {{ Auth::user()->user_name }}</h5>
          <h5 class="card-title">Email : {{ Auth::user()->email }}</h5>
          <h5 class="card-title">Position : {{ Auth::user()->profiles->position }}</h5>
          

        </div>
      </div>
@endsection