@extends('layout.master');

@section('title')
    Score 
@endsection

@section('content')
    <div class="container">
        <h1 class="mb-4">STATUS</h1>

        <a href="status/create" class="btn btn-primary">Add Status</a>

        <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Status Name</th>
                <th scope="col">action</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($statuses as $key=>$status)
              <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $status->status_name }}</td>
                <td>
                    <a href="/status/{{ $status->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <form action="/status/{{ $status->id }} " method="POST" class="d-inline" data-confirm-delete="true">
                        @csrf
                        @method('DELETE')
                     
                        <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button>
                    
                    </form>
                   

                </td>
              </tr>    
              @empty
                  
              @endforelse
              
            </tbody>
          </table>
    </div>
    @include('sweetalert::alert')

@endsection