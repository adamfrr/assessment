@extends('layout.master');

@section('title')
    Status | Create
@endsection

@section('content')
  <form action="/status" method="POST">
    @csrf
    <h1>Add Status</h1>
    <div class="mb-3">
      <label for="status_name" class="form-label">Status Name : </label>
      <input type="text" class="form-control" id="status_name" name="status_name">
    </div>
    @error('status_name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection