@extends('layout.master')

@section('title')
    Score | Edit
@endsection

@section('content')
<form action="/status/{{ $statuses->id }}" method="POST">
    @csrf
    @method('PUT')
    <h1>Edit Status</h1>
    <div class="mb-3">
      <label for="status_name" class="form-label">Status Name : </label>
      <input type="text" class="form-control" id="status_name" name="status_name" value="{{ $statuses->status_name }}">
    </div>
    @error('status_name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection