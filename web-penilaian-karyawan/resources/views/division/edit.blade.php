@extends('layout.master')

@section('title')
    Divisions | Edit    
@endsection

@section('content')
<form action="/division/{{ $divisions->id }}" method="POST">
    @csrf
    @method('PUT')
    <h1>Edit Division</h1>
    <div class="mb-3">
      <label for="division_name" class="form-label">Division Name : </label>
      <input type="text" class="form-control" id="division_name" name="division_name" value="{{ $divisions->division_name }}">
    </div>
    @error('division_name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection

