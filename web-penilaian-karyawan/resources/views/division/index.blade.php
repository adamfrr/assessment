@extends('layout.master')

@section('title')
    Division
@endsection

@section('content')
<h1 class="mb-4">DIVISIONS</h1>

<a href="division/create" class="btn btn-primary">Add Division</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Position Name</th>
        <th scope="col">action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($divisions as $key=>$division)
      <tr>
        <th scope="row">{{ $key + 1 }}</th>
        <td>{{ $division->division_name }}</td>
        <td>
            <a href="/division/{{ $division->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
            <form action="/division/{{ $division->id }} " method="POST" class="d-inline" data-confirm-delete="true">
                @csrf
                @method('DELETE')    
                <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button>
            </form>
        </td>
      </tr>    
      @empty
          <h1>No Data</h1>
      @endforelse
      
    </tbody>
  </table>
</div>
@include('sweetalert::alert')
@endsection