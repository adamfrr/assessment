@extends('layout.master')

@section('title')
    Division | Create
@endsection

@section('content')
<form action="/division" method="POST">
    @csrf
    <h1>Add Division</h1>
    <div class="mb-3">
      <label for="division_name" class="form-label">Division Name : </label>
      <input type="text" class="form-control" id="division_name" name="division_name">
    </div>
    @error('division_name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection