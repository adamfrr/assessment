@extends('layout.master')

@section('title')
    User |create
@endsection

@section('content')
    <h1>Create New User</h1>
    <form action="/user" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
          <label for="user_name" class="form-label">Username </label>
          <input type="text" class="form-control" id="user_name" name="user_name">
        </div>
        @error('user_name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="mb-3">
          <label for="email" class="form-label">Email </label>
          <input type="email" class="form-control" id="email" name="email">
        </div>
        @error('email')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="mb-3">
          <label for="password" class="form-label">Password </label>
          <input type="password" class="form-control" id="password" name="password">
        </div>
        @error('password')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="mb-3">
            <label for="name" class="form-label">Name </label>
            <input type="text" class="form-control" id="name" name="name">
          </div>
          @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror

        <div class="mb-3">
            <label for="position" class="form-label">Position </label>
            <select name="position" id="position" class="form-control">
              <option value="">--Select Position</option>
              <option value="Junior-Manager">Junior-Manager</option>
              <option value="Manager">Manager</option>
              <option value="Senior-Manager">Senior-Manager</option>
            </select>
          </div>
          @error('position')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror


        <button type="submit" class="btn btn-primary">Add New User</button>
      </form>
    
@endsection