@extends('layout.master')

@section('title')
    Users
@endsection

@section('content')
<h1>List User</h1>
<a href="/user/create" class="btn btn-primary">Add New User</a>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Username</th>
        <th scope="col">Position</th>
        <th scope="col">Email</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($users as $key=>$user)
      <tr>
        <th scope="row">{{ $key + 1 }}</th>
        <td>{{ $user->profiles->name }}</td>
        <td>{{ $user->user_name }}</td>
        <td>{{ $user->profiles->position}}</td>
        <td>{{ $user->email }}</td>
        <td>
            <a href="user/{{ $user->id }}/edit" class="btn btn-warning btn-sm">Update</a>
          
            <a href="change-password/{{ $user->id }}" class="btn btn-dark btn-sm">Change Password</a>
            @if ($user->profiles->position != 'Admin')
            <form action="/user/{{ $user->id }}" method="POST" class="d-inline">
              @csrf
              @method('DELETE')
              <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button>
            </form>
            @endif
        </td>

      </tr>    
      @empty
          <h1>No Data</h1>
      @endforelse
      
    </tbody>
  </table>
</div>
@include('sweetalert::alert')

@endsection