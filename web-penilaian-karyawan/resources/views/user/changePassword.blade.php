@extends('layout.master')

@section('title')
    User | Change Password
@endsection

@section('content')
<form action="/change-password/{{ $users->id }}" method="POST">
    @csrf
    @method('PUT')
    <h1>Change Password</h1>
    <div class="mb-3">
      <label for="old_password" class="form-label">Old Password </label>
      <input type="password" class="form-control" id="old_password" name="old_password">
    </div>
    @if(Session::has('error'))
    <div class="alert alert-danger">{{ Session::get('error') }}</div>
    @endif
    @error('old_password')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <div class="mb-3">
      <label for="new_password" class="form-label">New Password </label>
      <input type="password" class="form-control" id="new_password" name="new_password">
    </div>
    @error('new_password')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
      <label for="repeat_password" class="form-label">Repeat Password </label>
      <input type="password" class="form-control" id="repeat_password" name="repeat_password">
    </div>
    @error('repeat_password')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection