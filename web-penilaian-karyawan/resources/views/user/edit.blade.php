@extends('layout.master')

@section('title')
    User | Edit
@endsection

@section('content')
<form action="/user/{{ $users->id }}" method="POST">
    @csrf
    @method('PUT')
    <div class="mb-3">
      <label for="user_name" class="form-label">Username </label>
      <input type="text" class="form-control" id="user_name" name="user_name" value="{{ $users->user_name }}">
    </div>
    @error('user_name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
      <label for="email" class="form-label">Email </label>
      <input type="email" class="form-control" id="email" name="email" value="{{ $users->email }}">
    </div>
    @error('email')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
        <label for="name" class="form-label">Name </label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $users->profiles->name }}">
      </div>
      @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="mb-3">
        <label for="position" class="form-label">Position </label>
        <select name="position" id="position" class="form-control">
          <option value="">--Select Position</option>
          @if ($users->profiles->position == "Junior-Manager")
            <option selected value="Junior-Manager">Junior-Manager</option>    
            <option value="Manager">Manager</option>    
            <option value="Senior-Manager">Senior-Manager</option>    
            
          @elseif ($users->profiles->position == "Manager")
            <option selected value="Manager">Manager</option>
            <option value="Junior-Manager">Junior-Manager</option>
            <option value="Senior-Manager">Senior-Manager</option>
          
          @elseif ($users->profiles->position == "Admin")
            <option selected value="Admin">Admin</option>
          @else
            <option selected value="Senior-Manager">Senior-Manager</option>
            <option value="Junior-Manager">Junior-Manager</option>
            <option value="Manager">Manager</option>
          @endif
        </select>
      </div>
      @error('position')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror


    <button type="submit" class="btn btn-primary">Update User</button>
  </form>
@endsection