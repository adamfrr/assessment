@extends('layout.master')

@section('title')
    Employee | Detail
@endsection

@section('content')
    <h1 class="mb-3 text-center">Detail Employee</h1>
    <div class="container row">
        <div class="card col-12" >
            <img src="{{ asset('image/'.$employees->photo) }}" class="card-img-top mx-auto" alt="..." style="width: 30rem;">
            <div class="card-body">
              <h5 class="card-title">{{ $employees->name }}</h5>
              <p class="card-text">{{ $employees->address }}</p>
              <p class="card-text">{{ $employees->date_of_birth }}</p>
              <p class="card-text">{{ $employees->nik }}</p>
              <p class="card-text">{{ $employees->phone }}</p>
              <p class="card-text">{{ $employees->email }}</p>
              <p class="card-text">@if ( $employees->gender  == "F" )
                  Female
              @else
                  Men
              @endif</p>
              <p class="card-text">{{ $employees->education }}</p>
              <p class="card-text">{{ $employees->division->division_name }}</p>
              <p class="card-text">{{ $employees->status->status_name }}</p>
              <p class="card-text">{{ $employees->position->position_name }}</p>
              <a href="/employee" class="btn btn-secondary">Back to list Employee</a>
            </div>
          </div>
    </div>
@endsection