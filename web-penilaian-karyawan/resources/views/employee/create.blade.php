@extends('layout.master')

@section('title')
    Employee | Create
@endsection

@section('content')
<form action="/employee" method="POST" enctype="multipart/form-data">
    @csrf
    <h1>Add Employee</h1>
    <div class="mb-3">
      <label for="name" class="form-label">Name : </label>
      <input type="text" class="form-control" id="name" name="name">
    </div>
    @error('name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
      <label for="address" class="form-label">Address : </label>
      <textarea name="address" id="address" class="form-control"></textarea>
    </div>
    @error('address')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
        <label for="date_of_birth" class="form-label">Date Of Birth : </label>
        <input type="date" class="form-control" id="date_of_birth" name="date_of_birth">
      </div>
      @error('date_of_birth')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="mb-3">
        <label for="nik" class="form-label">NIK : </label>
        <input type="text" class="form-control" id="nik" name="nik">
      </div>
      @error('nik')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="mb-3">
        <label for="photo" class="form-label">Photo : </label>
        <input type="file" class="form-control" id="photo" name="photo">
      </div>
      @error('photo')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="mb-3">
        <label for="phone" class="form-label">Phone : </label>
        <input type="integer" class="form-control" id="phone" name="phone">
      </div>
      @error('phone')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="mb-3">
        <label for="email" class="form-label">Email : </label>
        <input type="text" class="form-control" id="email" name="email">
      </div>
      @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
      <div class="mb-3">
        <label for="gender" class="form-label">Gender : </label>
        <select name="gender" id="gender" class="form-control">
            <option value="">--Select Gender</option>
            <option value="M">M</option>
            <option value="F">F</option>
        </select>
      </div>
      @error('gender')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="mb-3">
        <label for="education" class="form-label">Education : </label>
        <input type="text" class="form-control" id="education" name="education">
      </div>
      @error('education')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="mb-3">
        <label for="division" class="form-label">Division : </label>
        <select name="division_id" id="division" class="form-control">
            <option value="">--Select Division</option>
            @forelse ($divisions as $division)
            <option value="{{ $division->id }}">{{ $division->division_name }}</option>
            @empty
            <h1>No Division</h1>
            @endforelse
        </select>
      </div>
      @error('division_id')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="mb-3">
        <label for="status" class="form-label">Status : </label>
        <select name="status_id" id="status" class="form-control">
            <option value="">--Select Status</option>
            @forelse ($statuses as $status)
            <option value="{{ $status->id }}">{{ $status->status_name }}</option>
            @empty
            <h1>No Status</h1>
            @endforelse
        </select>
      </div>
      @error('status_id')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="mb-3">
        <label for="position" class="form-label">Position : </label>
        <select name="position_id" id="position" class="form-control">
            <option value="">--Select Position</option>
            @forelse ($positions as $position)
            <option value="{{ $position->id }}">{{ $position->position_name }}</option>
            @empty
            <h1>No Position</h1>
            @endforelse
        </select>
      </div>
      @error('status_id')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Add Data</button>
  </form>
@endsection