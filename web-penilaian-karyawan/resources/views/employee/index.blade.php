@extends('layout.master')

@section('title')
    Emloyee
@endsection

@section('content')
    <h1>List Employee</h1>
    <a href="/employee/create" class="btn btn-primary mb-3" >Add Employee</a>
    <div class="row">
        @forelse ($employees as $employee)
        <div class="col-4">
           
            <div class="card" >
                <img src="{{ asset('image/'.$employee->photo) }}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">Name: {{ $employee->name }}</h5>
                  <p class="card-text">Address: {{ $employee->address }}</p>
                  <p class="card-text">Education: {{ $employee->education }}</p>
                  <div class="d-flex justify-content-between">
                    <a href="employee/{{ $employee->id }}" class="btn btn-primary">Detail</a>
                    <a href="employee/{{ $employee->id }}/edit" class="btn btn-warning">Edit</a>
                    <form action="employee/{{ $employee->id }}" method="POST">
                    @csrf
                    @method("DELETE")
                    <button class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
                    </form>
                  </div>
                </div>
            </div>
        </div>
        @empty
        @endforelse
    </div>
@include('sweetalert::alert')
@endsection

