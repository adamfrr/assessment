@extends('layout.master')

@section('title')
    
@endsection

@section('content')
<form action="/position/{{ $positions->id }}" method="POST">
    @csrf
    @method('PUT')
    <h1>Edit Status</h1>
    <div class="mb-3">
      <label for="position_name" class="form-label">Position Name : </label>
      <input type="text" class="form-control" id="position_name" name="position_name" value="{{ $positions->position_name }}">
    </div>
    @error('position_name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection