@extends('layout.master')

@section('title')
    Position | Create
@endsection

@section('content')
<form action="/position" method="POST">
    @csrf
    <h1>Add Position</h1>
    <div class="mb-3">
      <label for="position_name" class="form-label">Position Name : </label>
      <input type="text" class="form-control" id="position_name" name="position_name">
    </div>
    @error('position_name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection