@extends('layout.master')

@section('title')
    Position
@endsection

@section('content')
<div class="container">
    <h1 class="mb-4">POSITION</h1>

    <a href="position/create" class="btn btn-primary">Add position</a>

    <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Position Name</th>
            <th scope="col">action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($positions as $key=>$position)
          <tr>
            <th scope="row">{{ $key + 1 }}</th>
            <td>{{ $position->position_name }}</td>
            <td>
                <a href="/position/{{ $position->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                <form action="/position/{{ $position->id }} " method="POST" class="d-inline" data-confirm-delete="true">
                    @csrf
                    @method('DELETE')    
                    <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button>
                </form>
            </td>
          </tr>    
          @empty
              <h1>No Data</h1>
          @endforelse
          
        </tbody>
      </table>
</div>
@include('sweetalert::alert')
@endsection