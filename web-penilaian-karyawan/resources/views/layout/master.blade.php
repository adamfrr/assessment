<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>
  <link rel="shortcut icon" type="image/png" href="{{asset('template/assets/images/logos/favicon.png')}}" />
  <link rel="stylesheet" href="{{asset('template/assets/css/styles.min.css')}}" />
</head>

<body>
  <!--  Body Wrapper -->
  <div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
    data-sidebar-position="fixed" data-header-position="fixed">
    <!-- Sidebar Start -->
    @include('partial.slidebar')
    <!--  Sidebar End -->
    <!--  Main wrapper -->
    <div class="body-wrapper">
      <!--  Header Start -->
      @include('partial.header')
      <!--  Header End -->
      <div class="container-fluid">
        <!--  Row 1 -->
        <div class="row">
          <main style="min-height: 75vh">
            @yield('content')
          </main>
        </div>
        <div class="py-6 px-6 text-center">
          <p class="mb-0 fs-4">Design and Developed by <a href="https://adminmart.com/" target="_blank" class="pe-1 text-primary text-decoration-underline">AdminMart.com</a></p>
        </div>
      </div>
    </div>
  </div>
  <script src="{{asset('template/assets/libs/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('template/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('template/assets/js/sidebarmenu.js')}}"></script>
  <script src="{{asset('template/assets/js/app.min.js')}}"></script>
  <script src="{{asset('template/assets/libs/apexcharts/dist/apexcharts.min.js')}}"></script>
  <script src="{{asset('template/assets/libs/simplebar/dist/simplebar.js')}}"></script>
  <script src="{{asset('tamplate/assets/js/dashboard.js')}}"></script>
</body>

</html>