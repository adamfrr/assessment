<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::all();
        return view('user.index',[
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'user_name'=>'required',
            'email'=>'required',
            'password'=>'required',
        ]);

        $users = new User();
        $users->user_name = $request->user_name;
        $users->email = $request->email;
        
        $password = $request->password;
        $hashPw = Hash::make($password);
        $users->password = $hashPw;
   
        $users->save();
        

        $this->validate($request,[
            'name' => 'required',
            'position' => 'required',
        
        ]);


        $profiles = new Profile();
        $profiles->name = $request->name;
        $profiles->position = $request->position;
        $profiles->user_id = $users->id;

        $profiles->save();

        return redirect('/user')->with('success', 'Task Created Successfully!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $users = User::find($id);
        return view('user.edit',[
            'users' => $users,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request,[
            "user_name" => "required",
            "email" => "required",

        ]);

        $users = User::find($id);

        $users->user_name = $request->user_name;
        $users->email = $request->email;


        $users->save();

        $this->validate($request,[
            "name" => "required",
            "position" => "required",
        ]);
        $id_profile = ($users->id);
        
        
        $profiles = Profile::where('user_id',$id_profile)->first();

        $profiles->name = $request->name;
        $profiles->position = $request->position;

        $profiles->save();

        return redirect('/user')->with('success', 'Task Created Successfully!');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $users = User::find($id);
        $id_profile = $users->id;
        $profiles = Profile::where('user_id',$id_profile)->first();
        $profiles->delete();
        $users->delete();

        return redirect('/user');
    }

    public function changePassword($id)
    {
        $users = User::find($id);
        return view('user.changePassword',[
            'users' => $users
        ]);
    }

    public function updatePassword(Request $request,$id)
    {

        $this->validate($request,[
            'old_password' => 'required',
            'new_password' => 'min:3',
            'repeat_password' => 'required_with:new_password|same:new_password|min:3'
        ]);
        $users = User::find($id);
        $check = Hash::check($request->old_password, $users->password);
        if (!$check) {
            return back()->with('error','password salah');
        }

        $password = $request->new_password;
        $hashPw = Hash::make($password);
        $users->password = $hashPw;
        $users->save();

        return redirect('/user')->with('success', 'Task Created Successfully!');
    }
}
