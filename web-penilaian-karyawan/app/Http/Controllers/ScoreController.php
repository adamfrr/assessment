<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Score;
use App\Models\User;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Foreach_;

class ScoreController extends Controller
{


    /**
     * Display a listing of the resource.
     */
    public function __construct()
    {
        // $this->middleware('auth');
        //$this->middleware('manager')->only('index');
     $this->middleware('admin')->except(['index','add','store','detail']);
    }
    public function index()
    {
        $employees = Employee::paginate(4);
        
        
        $scores = Score::paginate(3);
        // foreach($scores as $score){
        //     $user_id = $score->user_id;
        //     $user_data = Employee::find($user_id);
        // }

        return view('score.index',[
            'employees' => $employees,
            'scores' => $scores,
            // 'user_data' => $user_data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request,$id)
    {
        return view('score.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'employee_id'=>'required',
            'user_id'=>'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'value' => 'required',
            'note' => 'required'
        ]);

        $scores = new Score();
        $scores->employee_id = $request->employee_id;
        $scores->user_id = $request->user_id;
        $scores->start_date = $request->start_date;
        $scores->end_date = $request->end_date;
        $scores->value = $request->value;
        $scores->note = $request->note;

        $scores->save();

        return redirect('/score')->with('success', 'Task Created Successfully!');;
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {   
        $scores = Score::find($id);
        return view('score.edit',[
            'scores' => $scores,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request,[
            'employee_id'=>'required',
            'user_id'=>'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'value' => 'required',
            'note' => 'required'
        ]);

        $scores = Score::find($id);

        $scores->employee_id = $request->employee_id;
        $scores->user_id = $request->user_id;
        $scores->start_date = $request->start_date;
        $scores->end_date = $request->end_date;
        $scores->value = $request->value;
        $scores->note = $request->note;

        $scores->save();

        return redirect('/score')->with('success', 'Task Created Successfully!');;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $scores = Score::find($id);

        $scores->delete();

        return redirect('/score');
    }

    public function add($id)
    {
    
        $employees = Employee::find($id);
        return view('score.add',[
            'employees' => $employees
        ]);
    }
    public function detail($id)
    {
        $employees = Employee::find($id);
        return view('score.detail',[
            'employees' => $employees
        ]);
    }
}
