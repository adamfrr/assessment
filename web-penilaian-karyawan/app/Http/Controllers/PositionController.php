<?php

namespace App\Http\Controllers;

use App\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $positions = Position::all();
        return view('position.index',[
            'positions' => $positions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('position.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'position_name' => 'required'
        ]);
        $position = new Position();
 
        $position->position_name = $request->position_name;
 
        $position->save();
 
        return redirect('/position')->with('success', 'Task Created Successfully!');
    
    }

    /**
     * Display the specified resource.
     */
    // public function show(string $id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $positions = Position::find($id);
        return view('position.edit',[
            'positions'=>$positions
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'position_name' => 'required'
        ]);
        $positions = Position::find($id);
 
        $positions->position_name = $request->position_name;
 
        $positions->save();

        return redirect('/position')->with('success', 'Task Created Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        
        $positions = Position::find($id);
        $positions->delete();
        return redirect('/position');

    }
}
