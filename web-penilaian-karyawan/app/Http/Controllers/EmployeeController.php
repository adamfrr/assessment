<?php

namespace App\Http\Controllers;

use App\Models\Divison;
use App\Models\Employee;
use App\Models\Position;
use App\Models\Status;
use Illuminate\Http\Request;
use File;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $employees = Employee::all();
        return view('employee.index',[
            'employees' => $employees
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $divisions = Divison::all();
        $positions = Position::all();
        $statuses = Status::all();
        return view('employee.create',[
            'divisions' => $divisions,
            'positions' => $positions,
            'statuses' => $statuses,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'address'=>'required',
            'date_of_birth'=>'required',
            'nik'=>'required',
            'photo'=>'required|image|mimes:jpg,png,jpeg',
            'phone'=>'required',
            'email'=>'required',
            'gender'=>'required',
            'education'=>'required',
            'division_id'=>'required',
            'status_id'=>'required',
            'position_id'=>'required',
        ]);
        //upload gambar
        $filename = time().'.'.$request->photo->extension();
        $request->photo->move(public_path('image'),$filename);

        $employe = new Employee();
        $employe->name = $request->name;
        $employe->address = $request->address;
        $employe->date_of_birth = $request->date_of_birth;
        $employe->nik = $request->nik;
        $employe->photo = $filename;
        $employe->phone = $request->phone;
        $employe->email = $request->email;
        $employe->gender = $request->gender;
        $employe->education = $request->education;
        $employe->division_id = $request->division_id;
        $employe->status_id = $request->status_id;
        $employe->position_id = $request->position_id;

        $employe->save();

        return redirect('/employee')->with('success', 'Task Created Successfully!');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $employees = Employee::with('status','division','position')->find($id);
        return view('employee.show',[
            'employees' => $employees
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $employees = Employee::find($id);
        $divisions = Divison::all();
        $positions = Position::all();
        $statuses = Status::all();
        return view('employee.edit',[
            'employees' => $employees,
            'divisions' => $divisions,
            'positions' => $positions,
            'statuses' => $statuses
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'address'=>'required',
            'date_of_birth'=>'required',
            'nik'=>'required',
            'photo'=>'image|mimes:jpg,png,jpeg',
            'phone'=>'required',
            'email'=>'required',
            'gender'=>'required',
            'education'=>'required',
            'division_id'=>'required',
            'status_id'=>'required',
            'position_id'=>'required',
        ]);

        $employees = Employee::find($id);

        //untuk update gambar
        if($request->has('photo')){
            $path = 'image/';
            File::delete($path. $employees->photo);
            
            $filename = time().'.'.$request->photo->extension();
            $request->photo->move(public_path('image'),$filename);

            $employees->photo = $filename;

            $employees->save();
            
        }

        $employees->name = $request->name;
        $employees->address = $request->address;
        $employees->date_of_birth = $request->date_of_birth;
        $employees->nik = $request->nik;
        $employees->phone = $request->phone;
        $employees->email = $request->email;
        $employees->gender = $request->gender;
        $employees->education = $request->education;
        $employees->division_id = $request->division_id;
        $employees->status_id = $request->status_id;
        $employees->position_id = $request->position_id;

        $employees->save();

        return redirect('/employee')->with('success', 'Task Created Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
      $employees = Employee::find($id);
      
      $path = 'image/';
            File::delete($path. $employees->photo);

            $employees->delete();
        return redirect('/employee');
    }
}
