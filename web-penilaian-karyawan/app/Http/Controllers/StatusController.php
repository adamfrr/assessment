<?php

namespace App\Http\Controllers;

use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $statuses = Status::all();
        
        return view('status.index',[
            'statuses' => $statuses
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('status.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'status_name' => 'required'
        ]);
        $status = new Status();
 
        $status->status_name = $request->status_name;
 
        $status->save();
 
        return redirect('/status')->with('success', 'Task Created Successfully!');
    }

    /**
     * Display the specified resource.
     */
    // public function show(string $id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $statuses = Status::find($id);
        return view('status.edit',[
            'statuses' => $statuses
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'status_name' => 'required'
        ]);

        $statuses = Status::find($id);
        $statuses->status_name = $request->status_name;
        $statuses->save();

        return redirect('/status')->with('success', 'Task Created Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {

        $statuses = Status::find($id);
        $statuses->delete();
        return redirect('/status');

    }
}
