<?php

namespace App\Http\Controllers;

use App\Models\Divison;
use Illuminate\Http\Request;

class DivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $divisions = Divison::all();
        return view('division.index',[
            'divisions' => $divisions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('division.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'division_name' => 'required'
        ]);
        $divisions = new Divison();
 
        $divisions->division_name = $request->division_name;
 
        $divisions->save();
 
        return redirect('/division')->with('success', 'Task Created Successfully!');
    }

    /**
     * Display the specified resource.
     */
    // public function show(string $id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $divisions = Divison::find($id);
        return view('division.edit',[
            'divisions' => $divisions
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'division_name' => 'required'
        ]);
        $divisions = Divison::find($id);
 
        $divisions->division_name = $request->division_name;
 
        $divisions->save();
 
        return redirect('/division')->with('success', 'Task Created Successfully!');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $divisions = Divison::find($id);
        $divisions->delete();
        return redirect('/division');
    }
}
