<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;
    protected $table = 'employees';
    protected $fillable = [
        'name',
        'address',
        'date_of_birth',
        'nik',
        'photo',
        'phone',
        'email',
        'gender',
        'education',
        'division_id',
        'status_id',
        'position_id'
    ];

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class,'status_id');
    }
    public function division(): BelongsTo
    {
        return $this->belongsTo(Divison::class,'division_id');
    }
    public function position()
    {
        return $this->belongsTo(Position::class,'position_id');
    }

    public function scores()
    {
        return $this->hasMany(Score::class, 'employee_id');
    }
   
}
