<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Divison extends Model
{
    use HasFactory;
    protected $table = 'divisions';
    protected $fillable = [
        'division_name'
    ];

    public function employees(): HasMany
    {
        return $this->hasMany(Employee::class);
    }
}
