<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ScoreController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\DivisionController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::middleware(['auth'])->group(function () {
    //dashboard controller
    Route::get('/', [DashboardController::class,'index']);
    // //score controller
    Route::resource('/score', ScoreController::class);
    //add score
    Route::get('/score-add/{id}',[ScoreController::class, 'add']);
    //detail score
    Route::get('/score-detail/{id}',[ScoreController::class, 'detail']);
    

    //status controller
    Route::resource('/status', StatusController::class)->except(['show'])->middleware('admin');
    //position controller
    Route::resource('/position', PositionController::class)->except(['show'])->middleware('admin');
    //position controller
    Route::resource('/division', DivisionController::class)->except(['show'])->middleware('admin');
    //employee controller
    Route::resource('/employee', EmployeeController::class);

    //user controller
    Route::resource('/user', UserController::class)->middleware('admin');

    //change password 
    Route::get('/change-password/{id}', [UserController::class,'changePassword']);
    //update password
    Route::put('/change-password/{id}', [UserController::class,'updatePassword']);
});
//login controller
Route::get('/login',[AuthController::class,'login'])->name('login')->middleware('guest');
Route::post('/login',[AuthController::class,'authenticate'])->middleware('guest');

//logout controller
Route::get('/logout',[AuthController::class,'logout'])->middleware('auth');